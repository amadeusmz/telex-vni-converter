val from = arrayOf (
	"à", "á", "â", "ã", "ă", "ạ", "ả", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ắ", "ằ", "ẳ", "ẵ", "ặ",
	"đ", "è", "é", "ê", "ẹ", "ẻ", "ẽ", "ế", "ề", "ể", "ễ", "ệ", "ì", "í", "ĩ", "ỉ", "ị",
	"ò", "ó", "ô", "õ", "ơ", "ọ", "ỏ", "ố", "ồ", "ổ", "ỗ", "ộ", "ớ", "ờ", "ở", "ỡ", "ợ",
	"ù", "ú", "ũ", "ư", "ụ", "ủ", "ứ", "ừ", "ử", "ữ", "ự", "ý", "ỳ", "ỷ", "ỹ", "ỵ"
)

val to = arrayOf(
	"a2", "a1", "a6", "a4", "a8", "a5", "a3", "a61", "a62", "a63", "a64", "a65", "a81",
	"a82", "a83", "a84", "a85", "d9", "e2", "e1", "e6", "e5", "e3", "e4", "e61", "e62",
	"e63", "e64", "e65", "i2", "i1", "i4", "i3", "i5", "o2", "o1", "o6", "o4", "o7",
	"o5", "o3", "o61", "o62", "o63", "o64", "o65", "o71", "o72", "o73", "o74", "o75",
	"u2", "u1", "u4", "u7", "u5", "u3", "u71", "u72", "ửu73", "u74", "u75", "y1", "y2",
	"y3", "y4", "y5"
)

val toTelex = arrayOf(
	"af", "as", "aa", "ax", "aw", "aj", "ar", "aas", "aaf", "aar", "aax", "aaj", "aws",
	"awf", "awr", "awx", "awj", "dd", "ef", "es", "ee", "ej", "er", "ex", "ees", "eef",
	"eer", "eex", "eej", "if", "is", "ix", "ir", "ij", "of", "os", "oo", "ox", "ow",
	"oj", "or", "oos", "oof", "oor", "oox", "ooj", "ows", "owf", "owr", "owx", "owj",
	"uf", "us", "ux", "uw", "uj", "ur", "uws", "uwf", "uwr", "uwx", "uwj", "ys", "yf",
	"yr", "yx", "yj"
)

fun main() {
	var input = "TIẾNG VIỆT CÓ DẤU".lowercase()
	from.forEachIndexed { index, s ->
		input = input.replace(s, to[index])
		println("\"${s.uppercase()} => ${toTelex[index]}\",")
	}
	println(input)
}

